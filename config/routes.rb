Rails.application.routes.draw do
  #get 'portada/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'portada#index'
  get  'portada/login'
  get	'portada/registro'
  get 	'portada/detalle'
  get 	'portada/listar'
  get 	'portada/solicitudejercicios'
  get	'contenido/cambio_perfil'
  get	'contenido/consulta_saldo'
end
